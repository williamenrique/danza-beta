<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/conf/config.sistema.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/backend/core/src/helpers/funciones.php';
$modelo = new Class_consultas();
$db = db;
$banner = $modelo->consultBanner($db);
$acerca = $modelo->consultAcerca($db);
$personal = $modelo->consultPersonal($db);

?>

<!DOCTYPE html>
</body>
</html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title><?php echo $nombresistema ?></title>
		<!-- MDB icon -->
		<link rel="icon" href="assets/img/mdb-favicon.ico" type="image/x-icon">
		<!-- Font Awesome-->
		<link rel="stylesheet" href="assets/css/all.css">
		
		<!-- <link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet"> -->
		<!-- Google Fonts Roboto -->
		<link href="https://fonts.googleapis.com/css?family=Abel|Raleway:200,300i,400,700i,800&display=swap" rel="stylesheet">
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Material Design Bootstrap -->
		<link rel="stylesheet" href="assets/css/mdb.min.css">
		<!-- Your custom styles (optional) -->
		<link rel="stylesheet" href="assets/css/style.css">
		<style type="text/css">
			.card-comments img{width:4rem}
		</style>
	</head>
<body>
	<div class="box-float-news rounded-circle"><i class="fas fa-indent"></i></div>
	<div class="box-float-msj rounded-circle"><i class="far fa-envelope"></i></div>
	<div class="box-float-menu rounded-circle"><i class="fas fa-ellipsis-v"></i></div>
	<div class="header">
		<div class="box-header">
			<div class="container">
				<div class="nav-menu">
					<a href="#" class="cerrar">x</a>
					<a href="#" class="activo">Inicio</a>
					<a href="#" class="">Galeria</a>
					<a href="#" class="">Nosotros</a>
					<a href="#" class="">Noticias</a>
					<a href="#" class="">Contactenos</a>
				</div>
			</div>
		</div>
	</div>
	<div class="box-banner">
		<img src="assets/img/banner/<?php echo $banner['imagen']?>" class="img-fluid" alt="">
		<div class="box-social">
			<a href="<?php echo $banner['face'] ?>" target="_blank" class="face hoverable"><i class="fab fa-facebook-f"></i></a>
			<a class="twitter hoverable" type="button" role="button" href="<?php echo $banner['twitter'] ?>" target="_blank"><i class="fab fa-twitter"></i></a>
			<a class="mail hoverable" type="button" role="button" href="<?php echo $banner['correo'] ?>" target="_blank"><i class="far fa-envelope"></i></a>
			<a class="insta hoverable" type="button" role="button" href="<?php echo $banner['instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a>
		</div>
	</div>
	<main class="pt-2">
      <div class="container">
			<section class="">
				<div class="row">
					<div class="col-md-8 mb-4">
						<!-- card banner -->
						<div class="card mb-4 wow fadeIn card-banner">
							<img src="assets/img/banner/<?php echo$banner['imagen']?>" class="img-fluid" alt="">
							<div class="card-social">
								<a href="<?php echo $banner['face'] ?>" target="_blank" class="face hoverable"><i class="fab fa-facebook-f"></i></a>
								<a class="twitter hoverable" type="button" role="button" href="<?php echo $banner['twitter'] ?>" target="_blank"><i class="fab fa-twitter"></i></a>
								<a class="mail hoverable" type="button" role="button" href="<?php echo $banner['correo'] ?>" target="_blank"><i class="far fa-envelope"></i></a>
								<a class="insta hoverable" type="button" role="button" href="<?php echo $banner['instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a>
							</div>
						</div>
						<!-- acerca -->
						<div class="card mt-2 mb-4 wow fadeIn card-acerca">
							<div class="card-body">
								<div class="d-flex flex-wrap media mt-3">
									<img class="img-fluid mb-3 mx-auto z-depth-1 rounded" src="assets/img/acerca/<?php echo $acerca['imagen'] ?>" alt="Generic placeholder image">
									<div class="media-body text-center ">
										<h5 class="mt-0 font-weight-bold p-l-2">
											<?php echo $acerca['titulo_acerca'] ?>
										</h5>
										<p class="text-justify"><?php echo $acerca['acerca']?></p>
										<button class="btn btn-info btn-sm mt-2" type="button">Leer más</button>
									</div>
								</div>
							</div>	
						</div>
						<!--/.Card noticia-->	
						<div class="card mb-4 wow fadeIn card-noticia">
							<div class="card-body">
								<p class="h5">
									<a href="">Titulo de la noticia</a>
								</p>
								<blockquote class="blockquote">
									<p class="parrafo mb-0">
										Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus modi, ipsa ratione eius et placeat minus eos vel veritatis, temporibus, voluptates ducimus commodi doloremque adipisci dolor omnis rerum vero explicabo! Lorem ipsum dolor sit amet, consectetur adipisicing elit.
									</p>
									<footer class="blockquote-footer fecha-news">24/04/2020 a las 02:15pm</footer>
								</blockquote>
							</div>
						</div>
					</div><!-- grid colum -->
					<div class="col-md-4 mb-4">
						<div class="card blue-gradient mb-4 wow fadeIn box-mensaje">
							<div class="card-body text-center">
								<h4 class="mb-4">
									<strong>Danza Santa Teresa</strong>
								</h4>
								<p class="">
									<strong class="titulo"><?php echo $banner['titulo_msj']?></strong>
								</p>
								<p class="mb-4 ">
									<strong class="mensaje"><?php echo $banner['mensaje']?></strong>
								</p>
							</div><!--/.Card mensaje-->
						</div>
						<div class="card mb-4 wow fadeIn">
							<div class="card-header">Eventos pasados</div>
							<!--Card content-->
							<div class="card-body">
								<ul class="list-unstyled">
									<li class="media">
										<img class="d-flex mr-3" src="assets/img/acerca/<?php echo $acerca['imagen']?>" style="width: 100px" alt="Generic placeholder image">
										<div class="media-body">
											<a href="">
												<h5 class="mt-0 mb-1 font-weight-bold">List-based media object</h5>
											</a>
											Cras sit amet nibh libero, in gravida nulla (...)
										</div>
									</li>
									<li class="media my-4">
										<img class="d-flex mr-3" src="assets/img/acerca/<?php echo $acerca['imagen']?>" style="width: 100px" alt="An image">
										<div class="media-body">
											<a href="">
												<h5 class="mt-0 mb-1 font-weight-bold">List-based media object</h5>
											</a>
											Cras sit amet nibh libero, in gravida nulla (...)
										</div>
									</li>
									<li class="media">
										<img class="d-flex mr-3" src="assets/img/acerca/<?php echo $acerca['imagen']?>" style="width: 100px" alt="Generic placeholder image">
										<div class="media-body">
											<a href="">
												<h5 class="mt-0 mb-1 font-weight-bold">List-based media object</h5>
											</a>
											Cras sit amet nibh libero, in gravida nulla (...)
										</div>
									</li>
								</ul>
							</div>
						</div><!-- card relase-->
					</div><!-- grid colum 2-->
					<!--Card persona--> 
					<div class="row row-cols-1 row-cols-md-3"><!-- row  persona-->
						<!-- 1 -->
						<div class="col mb-4">
							<div class ="card card-personal mb-4 mr-2">
								<div class ="text-center">
									<img class =" rounded-circle card-img-top mt-2" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(26).jpg" alt="Card image cap" style="width: 150px">
									<a href="#!">
										<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<a>
										<h4 class="card-title">Anna</h4>
									</a>
									<a class="card-meta">Friends</a>
									<p class="card-text">Anna is a web designer living in New York.</p>
									<hr>
									<a class ="card-meta">
										<span><i class="fas fa-user"></i>83 Friends</span>
									</a>
									<p class="card-meta float-right">Joined in 2012</p>
								</div>
							</div>
						</div>
						<!-- 2 -->
						<div class="col mb-4">
							<div class ="card card-personal mb-4 mr-2">
								<div class ="text-center">
									<img class =" rounded-circle card-img-top mt-2" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(26).jpg" alt="Card image cap" style="width: 150px">
									<a href="#!">
										<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<a>
										<h4 class="card-title">Anna</h4>
									</a>
									<a class="card-meta">Friends</a>
									<p class="card-text">Anna is a web designer living in New York.</p>
									<hr>
									<a class ="card-meta">
										<span><i class="fas fa-user"></i>83 Friends</span>
									</a>
									<p class="card-meta float-right">Joined in 2012</p>
								</div>
							</div>
						</div>
						<!-- 3 -->
						<div class="col mb-4">
							<div class ="card card-personal mb-4 mr-2">
								<div class ="text-center">
									<img class =" rounded-circle card-img-top mt-2" src="https://mdbootstrap.com/img/Photos/Avatars/img%20(26).jpg" alt="Card image cap" style="width: 150px">
									<a href="#!">
										<div class="mask rgba-white-slight"></div>
									</a>
								</div>
								<div class="card-body">
									<a>
										<h4 class="card-title">Anna</h4>
									</a>
									<a class="card-meta">Friends</a>
									<p class="card-text">Anna is a web designer living in New York.</p>
									<hr>
									<a class ="card-meta">
										<span><i class="fas fa-user"></i>83 Friends</span>
									</a>
									<p class="card-meta float-right">Joined in 2012</p>
								</div>
							</div>
						</div>
						<!-- 2 -->
					</div><!-- row  persona-->




				</div><!-- row  principal-->
			</section><!-- section principal -->
		</div>
	</main>
	<footer class="site-footer">
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 text-lg-left text-center mb-lg-0">
							<p class="copyright">© 2020  All Rights Reserved. Design by <a
						href="#">William Infante</a>
						<a href="backend.php" target="_blank">Administrar</a> </p>
						</div>
						<div class="col-lg-4 box-social text-center">
							<a href="<?php echo $banner['face'] ?>" target="_blank" class="face hoverable"><i class="fab fa-facebook-f"></i></a>
							<a class="twitter hoverable" type="button" role="button" href="<?php echo $banner['twitter'] ?>" target="_blank"><i class="fab fa-twitter"></i></a>
							<a class="mail hoverable" type="button" role="button" href="<?php echo $banner['correo'] ?>" target="_blank"><i class="far fa-envelope"></i></a>
							<a class="insta hoverable" type="button" role="button" href="<?php echo $banner['instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
	<!-- jQuery -->
	<script type="text/javascript" src="assets/js/jquery.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="assets/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="assets/js/mdb.min.js"></script>
	<!-- Your custom scripts (optional) -->
	<script type="text/javascript"></script>
</body>
</html>