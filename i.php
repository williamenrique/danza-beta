<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/conf/config.sistema.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/backend/core/vendor/funciones.php';
$modelo = new Class_consultas();
$db = db;
$banner = $modelo->consultBanner($db);
$acerca = $modelo->consultAcerca($db);
$personal = $modelo->consultPersonal($db);

?>
<!DOCTYPE html>
</body>
</html>
<html lang="es">
	<head>
		<!-- Required meta tags -->
		<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<title><?php echo $nombresistema ?></title>
		<!-- MDB icon -->
		<link rel="icon" href="assets/img/mdb-favicon.ico" type="image/x-icon">
		<!-- Font Awesome-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
		
		<link href="https://fonts.googleapis.com/css?family=Abel&display=swap" rel="stylesheet">
		<!-- Google Fonts Roboto -->
		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<!-- Material Design Bootstrap -->
		<link rel="stylesheet" href="assets/css/mdb.min.css">
		<!-- Your custom styles (optional) -->
		<link rel="stylesheet" href="assets/css/style.css">
	</head>
		<style type="text/css">
		.banner{
			background: url('assets/img/banner/<?=$banner['imagen']?>') no-repeat  center;
			background-size: cover;
		}
		</style>

	</head>
	<body>
	</body>
		<div class="header">
			<div class="row box-header">
				<div class="container">
					<div class="nav-menu">
						<a href="#" class="cerrar">x</a>
						<a href="#" class="activo">Inicio</a>
						<a href="#" class="">Galeria</a>
						<a href="#" class="">Nosotros</a>
						<a href="#" class="">Noticias</a>
						<a href="#" class="">Contactenos</a>
					</div>
				</div>
			</div>
		</div>

		<div class="banner">
			<div class="container">
				<div class="row box-banner">
					<div class="box-mensaje">
						<h3><?php echo $banner['titulo_msj'] ?> </h3>
						<p><?php echo $banner['mensaje'] ?> </p>
					</div>
					<div class="box-social">
						<a href="<?php echo $banner['face'] ?>" target="_blank" class="face hoverable"><i class="fab fa-facebook-f"></i></a>
						<a class="twitter hoverable" type="button" role="button" href="<?php echo $banner['twitter'] ?>" target="_blank"><i class="fab fa-twitter"></i></a>
						<a class="mail hoverable" type="button" role="button" href="<?php echo $banner['correo'] ?>" target="_blank"><i class="far fa-envelope"></i></a>
						<a class="insta hoverable" type="button" role="button" href="<?php echo $banner['instagram'] ?>" target="_blank"><i class="fab fa-instagram"></i></a>
					</div>
				</div>
			</div>
		</div>
		
		<div class="main container">
			<div class="box-main">
				<div class="box-about">
					<div class="about">
						<h3 class="title">Acerca</h3>
				      <h6><?php echo $acerca['titulo_acerca']?></h6>
				      <p class="mt-md-4 mt-3 mb-0"><?php echo $acerca['acerca']?></p>
					</div>
					<div class="img-about ">
						<img src="assets/img/acerca/<?php echo $acerca['imagen']?>" alt="" class="shadow">
					</div>
				</div><!-- end acerca -->

			</div> 
			<!-- end box-main -->
			<div class="aside-news">
				<h4 class="small">Noticias</h4>
			</div>
		</div>
		<!-- end  main -->
		<!-- site footer -->
		<footer class="site-footer">
			<div class="bottom-footer">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 text-lg-left text-center mb-lg-0 mb-3">
							<p class="copyright">© 2020  All Rights Reserved. Design by <a
						href="https://w3layouts.com/">William Infante</a>
						<a href="backend.php" target="_blank">Administrar</a> </p>
						</div>
						<div class="col-lg-4 align-center text-lg-right text-center">
							<a href="#"><img src="assets/img/fb.png" title="facebook" alt=""></a>
							<a href="#"><img src="assets/img/twitter.png" title="twitter" alt="" ></a>
							<a href="#"><img src="assets/img/mail.png" title="mail	" alt=""></a>
							<a href="#"><img src="assets/img/feed.png" title="instagram" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</footer>
		<!-- jQuery -->
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<!-- Bootstrap tooltips -->
		<script type="text/javascript" src="assets/js/popper.min.js"></script>
		<!-- Bootstrap core JavaScript -->
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<!-- MDB core JavaScript -->
		<script type="text/javascript" src="assets/js/mdb.min.js"></script>
		<!-- Your custom scripts (optional) -->
		<script type="text/javascript"></script>
	</body>
</html>