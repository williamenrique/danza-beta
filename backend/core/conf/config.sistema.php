<?php
@session_start();
header('Content-type: text/html; charset=utf-8');
$servidor = "http://danza-beta.com/backend";
$sistema = "backend/core/src/";	
$nombresistema = "Danza beta";	// nombre del sistema

//constates del sistema
define('JS',$servidor.'/core/vendor/js/');
define('CSS',$servidor.'/core/vendor/css/');
define('css',$servidor.'/assets/css/');
define('js',$servidor.'/assets/js/');
define('img',$servidor.'/assets/images/');

define('RAIZ',$_SERVER["DOCUMENT_ROOT"].'/');
define('modules',$_SERVER["DOCUMENT_ROOT"].'/backend/modules/');
define('MODELS',$_SERVER["DOCUMENT_ROOT"].'/'.$sistema.'models/');
define('CONTROLLERS',$_SERVER["DOCUMENT_ROOT"].'/'.$sistema.'controllers/');
date_default_timezone_set('America/Caracas');

define('host' ,'localhost');
define('user' ,  'root');
define('pass' , 'root');
define('db' , 'db_danza');

define('METHOD','AES-256-CBC');
define('SECRET_KEY','$d@ns@');
define('SECRET_IV','101712');