  <div class="header sticky-header">
	<!-- notification menu start -->
	<div class="menu-right">
	  <div class="navbar user-panel-top">
		<div class="search-box">
		  <form action="#search-results.html" method="get">
			<input class="search-input" placeholder="Search Here..." type="search" id="search">
			<button class="search-submit" value=""><span class="fa fa-search"></span></button>
		  </form>
		</div>
		<div class="user-dropdown-details d-flex">
		<!--notification-->
		<?php include 'notificaciones.php'?>
		<?php include 'user_menu.php'?>
		</div>
	  </div>
	</div>
	<!--notification menu end -->
  </div>
  <!-- //header-ends -->