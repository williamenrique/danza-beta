<?php 
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/conf/config.sistema.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas_usuario.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/backend/core/src/model/Class_consultas.php';
require_once $_SERVER["DOCUMENT_ROOT"].'/backend/core/src/helpers/funciones.php';
$funciones = new Funciones();
$modelo = new Class_consultas_usuario();
$modeloConsultas = new Class_consultas();
$db = db;
$id_usuario = $_SESSION['id_usuario'];
$modelo->id_usuario = $id_usuario;
$consultDatos = $modelo->consultDatos($db);

$estado = $modeloConsultas->estado($db);

$nombre = 	$consultDatos['nombres'] ;
$apellidos = 	$consultDatos['apellidos'] ;
$email = 	$consultDatos['email'] ;
$clave = 	$consultDatos['clave_correo'] ;
$user = 	$consultDatos['user'] ;
$direccion = 	$consultDatos['direccion'] ;
$id_estado = 	$consultDatos['id_estado'] ;
$id_ciudad = 	$consultDatos['id_ciudad'] ;
$id_municipio = $consultDatos['id_municipio'] ;

$modeloConsultas->id_estado =$id_estado;
$modeloConsultas->id_ciudad =$id_ciudad;
$modeloConsultas->id_municipio =$id_municipio;
$modeloConsultas->id_usuario =$id_usuario;
$todoEstado = $modeloConsultas->todoEstado($db);
foreach ($todoEstado as $todo):
	$var_id_estado=  $todo['id_estado'];
	$var_estado=  $todo['estado'];
	$var_id_ciudad=  $todo['ciudad'];
	$var_ciudad=  $todo['ciudad'];
	$var_id_municipio = $todo['id_municipio'];
	$var_municipio = $todo['municipio'];
endforeach;
$pass = 	$funciones->decryption($consultDatos['pass']);
?>
<div class="form-update">
	<form id="form_update_user">
		<div class="media mb-2">
			<img src="../<?php echo $_SESSION['imagen_perfil'] ?>" class="mr-3" alt="...">
			<div class="media-body">
				<h5 class="mt-4">Cambie su imagen</h5>
			</div>
		</div>
		<div class="form-row">
			<div class="form-group col-lg-3 col-sm-6">
				<label for="emailUp">Email</label>
				<input type="email" class="form-control" id="emailUp" value="<?php echo $email?> ">
			</div>
			<div class="form-group col-lg-3 col-sm-6">
				<label for="passUp">Password Email</label>
				<input type="password" class="form-control" id="passUp" value="<?php echo $clave ?>">
			</div>
			<div class="form-group col-lg-3 col-sm-6">
				<label for="userUp">Usuario</label>
				<input type="text" class="form-control" id="userUp" value="<?php  echo $user ?> " readonly>
			</div>
			<div class="form-group col-lg-3 col-sm-6">
				<label for="claveUp">Clave</label>
				<input type="password" class="form-control" id="claveUp" name="claveUp" value="<?php  echo $pass ?>">
			</div>
		</div>
	 
	  
		<div class="form-row">
			<div class="form-group col-md-12">
				<label for="direccion">Dirección</label>
				<input type="text" class="form-control" id="direccion" value="<?php  echo $direccion?>" name="direccion" placeholder="Apartment, studio, or floor">
			</div>
			<div class="form-group  col-md-4 estado">
				<label for="inputState">Estado</label>
			<?php if($id_estado == ""):?>
				<select  class="form-control" name="id_estado" id="estado">
					<option selected>Seleccione</option>
			<?php foreach ($estado as $key):?>
					<option value="<?php echo $key['id_estado'] ?>"><?php echo $key['estado'] ?></option>
			<?php endforeach;?>
				</select>
			<?php else:?>
				<input type="text" class="form-control" id="estado" value="<?php  echo $var_estado?>" name="estado" readonly>
				<input type="hidden" class="form-control" id="id_estado" value="<?php  echo $var_id_estado?>" name="id_estado" readonly>
			<?php endif;?>
			</div>
			<div class="form-group  col-md-4 ciudad">
				<label for="inputState">Ciudad</label>
			<?php if($id_ciudad == ""):?>
				<select  class="form-control " id="ciudad" name="id_ciudad">
					<option selected>Seleccione</option>
				</select>
			<?php else:?>
				<input type="text" class="form-control" id="ciudad" value="<?php  echo $var_ciudad?>" name="ciudad" readonly>
				<input type="hidden" class="form-control" id="id_ciudad" value="<?php  echo $var_id_ciudad?>" name="id_ciudad" readonly>
			<?php endif;?>				
			</div>
			<div class="form-group  col-md-4 municipio">
				<label for="inputState">Municipio</label>
			<?php if($id_municipio == ""):?>
				<input type="hidden" class="form-control" name="id_municipio" id="id_municipio" readonly>
				<input type="text" class="form-control" name="municipio" id="municipio" readonly>
			<?php else:?>
				<input type="hidden" class="form-control" name="id_municipio" id="id_municipio" readonly value="<?php  echo $var_id_municipio?>">
				<input type="text" class="form-control" name="municipio" id="municipio" readonly value="<?php  echo $var_municipio?>">
			<?php endif;?>
			</div>
		</div>
	  <button type="button" onclick="actualizarUser()" class="btn btn-primary">Actualizar</button>
	  <input type="hidden" id="id_usuario" name="id_usuario" value="<?php echo $id_usuario?> ">
	</form>
</div>

<script>

</script>