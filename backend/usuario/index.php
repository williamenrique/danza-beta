<?php 
include '../modules/head.php';
?>

<div class="se-pre-con"></div>
	<section>
		<!-- sidebar menu start -->
		<?php include modules.'/sidebar.php' ?>
		<!-- //sidebar menu end -->
		<!-- header-starts -->
		<?php  include modules.'/header.php'?>
		<!-- //header-ends -->
		<!-- main content start -->
		<div class="main-content">
			<div class="container-fluid content-top-gap">
			<?php include 'cards.php' ?>
				<div class="box-cont">
				<?php include 'form_user.php' ?>
				</div>
			</div>
		</div>
		<!-- content -->
	</section>
<?php 
include modules.'/footer.php';
?>